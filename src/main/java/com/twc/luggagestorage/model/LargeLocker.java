package com.twc.luggagestorage.model;

import com.twc.luggagestorage.exception.InvalidTicketException;
import com.twc.luggagestorage.exception.NoEnoughCapacityException;

import java.util.HashMap;
import java.util.Map;

public class LargeLocker extends Locker {

    private int capacity;
    private Map<Ticket, Luggage> largeLocker = new HashMap<>();

    LargeLocker(int largeLockerCapacity) {
        this.capacity = largeLockerCapacity;
    }

    @Override
    public Ticket deposit(Luggage luggage) {
        if (largeLocker.size() >= capacity) {
            throw new NoEnoughCapacityException();
        }
        Ticket ticket = new Ticket(4);
        largeLocker.put(ticket, luggage);
        return ticket;
    }

    @Override
    public Luggage claimLuggage(Ticket ticket) {
        if (!largeLocker.containsKey(ticket)) {
            throw new InvalidTicketException();
        }
        return largeLocker.remove(ticket);
    }
}
