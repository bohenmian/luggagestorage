package com.twc.luggagestorage.model;

import com.twc.luggagestorage.enums.LuggageSizeEnum;

public class Luggage {

    private LuggageSizeEnum size;

    public Luggage() {
        this(LuggageSizeEnum.LARGE);
    }

    public Luggage(LuggageSizeEnum size) {
        this.size = size;
    }

    public static Luggage createExtraLargeLuggage() {
        return new Luggage(LuggageSizeEnum.EXTRA_LARGE);
    }

    public LuggageSizeEnum getLuggageSize() {
        return this.size;
    }
}
