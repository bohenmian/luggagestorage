package com.twc.luggagestorage.model;

public abstract class Locker {

    public abstract Ticket deposit(Luggage luggage);

    public abstract Luggage claimLuggage(Ticket ticket);
}
