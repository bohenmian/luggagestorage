package com.twc.luggagestorage.model;

import com.twc.luggagestorage.exception.InvalidTicketException;
import com.twc.luggagestorage.exception.NoEnoughCapacityException;

import java.util.HashMap;
import java.util.Map;

public class MediumLocker extends Locker {

    private int capacity;
    private Map<Ticket, Luggage> mediumLocker = new HashMap<>();

    MediumLocker(int mediumLockerCapacity) {
        this.capacity = mediumLockerCapacity;
    }

    @Override
    public Ticket deposit(Luggage luggage) {
        if (mediumLocker.size() >= capacity) {
            throw new NoEnoughCapacityException();
        }
        Ticket ticket = new Ticket(3);
        mediumLocker.put(ticket, luggage);
        return ticket;
    }

    public Luggage claimLuggage(Ticket ticket) {
        if (!mediumLocker.containsKey(ticket)) {
            throw new InvalidTicketException();
        }
        return this.mediumLocker.remove(ticket);
    }
}
