package com.twc.luggagestorage.model;

public class Ticket {

    private int status;

    public Ticket(int status) {
        this.status = status;
    }

    int getStatus() {
        return status;
    }
}
