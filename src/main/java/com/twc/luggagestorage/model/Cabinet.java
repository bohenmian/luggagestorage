package com.twc.luggagestorage.model;

import com.twc.luggagestorage.enums.LockerSizeEnum;
import com.twc.luggagestorage.exception.SizeTooLargeException;

import java.util.Objects;

public class Cabinet {

    private LargeLocker largeLocker;
    private MediumLocker mediumLocker;

    public Cabinet() {
        this(3, 3);
    }

    public Cabinet(int capacity) {
        this(capacity, capacity);
    }

    public Cabinet(int largeLockerCapacity, int mediumLockerCapacity) {
        this.largeLocker = new LargeLocker(largeLockerCapacity);
        this.mediumLocker = new MediumLocker(mediumLockerCapacity);
    }

    public Ticket deposit(Luggage luggage, LockerSizeEnum lockSize) {
        if (Objects.nonNull(luggage) && !lockSize.isLagerThanLuggageSize(luggage)) {
            throw new SizeTooLargeException();
        }
        return lockSize.equals(LockerSizeEnum.MEDIUM) ?
                this.mediumLocker.deposit(luggage) : this.largeLocker.deposit(luggage);
    }

    public Luggage claimLuggage(Ticket ticket) {
        return ticket.getStatus() == 4 ? this.largeLocker.claimLuggage(ticket) :
                this.mediumLocker.claimLuggage(ticket);
    }


}
