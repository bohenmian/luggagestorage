package com.twc.luggagestorage.enums;

import com.twc.luggagestorage.model.Luggage;

public enum LockerSizeEnum {

    LARGE(4), MEDIUM(3);

    private int weight;

    LockerSizeEnum(int weight) {
        this.weight = weight;
    }


    public boolean isLagerThanLuggageSize(Luggage luggage) {
        return this.weight >= luggage.getLuggageSize().getWeight();
    }

}
