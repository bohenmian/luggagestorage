package com.twc.luggagestorage.enums;

public enum LuggageSizeEnum {

    EXTRA_LARGE(5), LARGE(4), MEDIUM(3), SMALL(2), MINI(1);

    private int weight;

    LuggageSizeEnum(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}
