package com.twc.luggagestorage;

import com.twc.luggagestorage.enums.LuggageSizeEnum;
import com.twc.luggagestorage.model.Luggage;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class LuggageSizeTest {


    @ParameterizedTest()
    @EnumSource(
            value = LuggageSizeEnum.class,
            names = {"EXTRA_LARGE", "LARGE", "MEDIUM", "SMALL", "MINI"}

    )
    void should_return_large_luggage_when_give_large(LuggageSizeEnum size) {
        Luggage luggage = new Luggage(size);
        assertEquals(size, luggage.getLuggageSize());
    }

}
