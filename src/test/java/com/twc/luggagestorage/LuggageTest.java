package com.twc.luggagestorage;


import com.twc.luggagestorage.enums.LockerSizeEnum;
import com.twc.luggagestorage.enums.LuggageSizeEnum;
import com.twc.luggagestorage.exception.InvalidTicketException;
import com.twc.luggagestorage.exception.NoEnoughCapacityException;
import com.twc.luggagestorage.exception.SizeTooLargeException;
import com.twc.luggagestorage.model.Cabinet;
import com.twc.luggagestorage.model.Luggage;
import com.twc.luggagestorage.model.Ticket;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class LuggageTest {


    private Cabinet cabinet;

    @BeforeEach
    void setUp() {
        cabinet = new Cabinet();
    }

    @Test
    void should_return_ticket_when_storage_luggage() {
        Ticket ticket = cabinet.deposit(new Luggage(), LockerSizeEnum.LARGE);
        assertNotNull(ticket);
    }

    @Test
    void should_claim_my_luggage_when_give_ticket() {
        Luggage myLuggage = new Luggage();
        Ticket ticket = cabinet.deposit(myLuggage, LockerSizeEnum.LARGE);

        Luggage claimedLuggage = cabinet.claimLuggage(ticket);
        assertEquals(myLuggage, claimedLuggage);
    }

    @Test
    void should_claim_my_first_luggage_when_give_ticket() {
        Luggage firstLuggage = new Luggage();
        Luggage secondLuggage = new Luggage();
        Ticket ticket = cabinet.deposit(firstLuggage, LockerSizeEnum.LARGE);
        cabinet.deposit(secondLuggage, LockerSizeEnum.LARGE);
        Luggage claimedLuggage = cabinet.claimLuggage(ticket);
        assertEquals(firstLuggage, claimedLuggage);
    }

    @Test
    void should_return_null_when_luggage_null() {
        Ticket ticket = cabinet.deposit(null, LockerSizeEnum.LARGE);
        Luggage claimedLuggage = cabinet.claimLuggage(ticket);
        assertNull(claimedLuggage);
    }

    @Test
    void should_throw_exception_when_ticket_is_valid() {
        assertThrows(InvalidTicketException.class, () -> cabinet.claimLuggage(new Ticket(4)));
    }

    @Test
    void should_throw_noEnoughCapacity_when_no_enough_capacity() {
        Cabinet cabinet = new Cabinet(3);

        IntStream.range(0, 3).forEach(i -> cabinet.deposit(new Luggage(), LockerSizeEnum.LARGE));

        assertThrows(NoEnoughCapacityException.class, () -> cabinet.deposit(new Luggage(), LockerSizeEnum.LARGE));
    }

    @Test
    void should_throw_exception_when_luggage_is_large_than_locker() {
        assertThrows(SizeTooLargeException.class, () -> cabinet.deposit(Luggage.createExtraLargeLuggage(), LockerSizeEnum.LARGE));
    }

    @Test
    void should_return_the_luggage_when_give_storage_locker_size_and_luggage_size_is_mini() {
        Cabinet cabinet = new Cabinet(3, 3);
        Luggage luggage = new Luggage(LuggageSizeEnum.MINI);
        Ticket ticket = cabinet.deposit(luggage, LockerSizeEnum.MEDIUM);
        Luggage claimLuggage = cabinet.claimLuggage(ticket);
        assertEquals(luggage, claimLuggage);
    }

    @Test
    void should_return_the_luggage_when_give_storage_locker_size_and_luggage_size() {
        Cabinet cabinet = new Cabinet(3, 3);
        Luggage luggage = new Luggage(LuggageSizeEnum.SMALL);
        Ticket ticket = cabinet.deposit(luggage, LockerSizeEnum.MEDIUM);
        Luggage claimLuggage = cabinet.claimLuggage(ticket);
        assertEquals(luggage, claimLuggage);
    }

    @Test
    void should_return_the_luggage_when_give_storage_locker_size_medium() {
        Cabinet cabinet = new Cabinet(3, 3);
        Luggage luggage = new Luggage(LuggageSizeEnum.MEDIUM);
        Ticket ticket = cabinet.deposit(luggage, LockerSizeEnum.MEDIUM);
        Luggage claimLuggage = cabinet.claimLuggage(ticket);
        assertEquals(luggage, claimLuggage);
    }

    @Test
    void should_throw_exception_when_give_storage_locker_size_less_than_luggage_size() {
        Cabinet cabinet = new Cabinet(3, 3);
        Luggage luggage = new Luggage(LuggageSizeEnum.LARGE);
        assertThrows(SizeTooLargeException.class, () -> cabinet.deposit(luggage, LockerSizeEnum.MEDIUM));
    }

    @Test
    void should_throw_no_enough_capacity_when_all_locker_is_full() {
        Cabinet cabinet = new Cabinet(3);
        IntStream.range(0, 3).forEach(i -> cabinet.deposit(new Luggage(LuggageSizeEnum.SMALL), LockerSizeEnum.MEDIUM));
        assertThrows(NoEnoughCapacityException.class, () -> cabinet.deposit(new Luggage(LuggageSizeEnum.MEDIUM), LockerSizeEnum.MEDIUM));

    }
}

